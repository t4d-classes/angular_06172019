Exercise 3

1. Collect the history of calc tool operations. In the history, collect the type of mathematical operation and the numeric value applied by the operation.

2. Display the history of operations under the calculator using an HTML unordered list. You may reuse the Unordered List component from the review exercise.

*ngFor="let item of items$ | async"

3. Add a button to clear the result and the history of mathematical operations.

4. Using the Redux DevTools, review the dispatched actions, changes in state and explore the time travel features.

4. Ensure it works.