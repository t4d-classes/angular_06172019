# Welcome to Advanced Angular

## Instructor

Eric Greene - [http://t4d.io](http://t4d.io)

## Schedule

Class:

- Monday through Wednesday: 9am to 5pm

Breaks:

- Morning Break: 10:25am to 10:35am
- Lunch: 12pm to 1pm
- Afternoon Break #1: 2:15pm to 2:25pm
- Afternoon Break #2: 3:40pm to 3:50pm

## Course Outline

- Day 1 - Review Assignment, Principles of Redux, ngRx/store (actions & reducers), Immutable Programming, ngRx/devtools, Selectors & Computed/Derived Values
- Day 2 - Asynchronous Programming & NgZone, Promises & RxJS, HttpClient, ngRx/effects, Router Store, Lazy Loading and Features
- Day 3 - NgContent, NgModel vs Reactive Forms with regards to Redux State, Custom Form Controls

### Instructor's Resources
****
- [ExitCertified](https://www.exitcertified.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)

## Useful Resources

- [Angular CLI](https://cli.angular.io/)
- [TypeScript Coding Guidelines](https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines)
- [Angular Style Guide](https://angular.io/docs/ts/latest/guide/style-guide.html)
- [Angular Cheat Sheet](https://angular.io/docs/ts/latest/guide/cheatsheet.html)
- [Angular API](https://angular.io/docs/ts/latest/api/)
