import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));


// import { Observable, Observer, Subscription } from 'rxjs';
// import { map, take } from 'rxjs/operators';

// const nums$ = Observable.create((observer: Observer<number>) => {

//   console.log('called observer');

//   let counter = 0;

//   const h = setInterval(() => {

//     if (observer.closed) {
//       console.log('is closed');
//       clearInterval(h);
//     }

//     console.log('setinterval executed: ', counter);
//     observer.next(counter++);
//   }, 500);

//   // setTimeout(() => {
//   //   clearInterval(h);
//   //   observer.complete();
//   // }, 10000);

// });

// const doubleNums$ = nums$.pipe(
//   map((x: number) => x * 1),
//   // take(5),
// );

// const doubleNumsSubscription: Subscription = doubleNums$.subscribe((num: number) => {
//   console.log(num);
// }, null, () => {
//   console.log('all done');
// });

// setTimeout(() => {
//   console.log('calling unsubscribe');
//   doubleNumsSubscription.unsubscribe();
// }, 5000);
