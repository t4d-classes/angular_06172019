import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Car } from '../../models/car';
import { CarToolState } from '../../car-tool.state';
import {AppendCarRequestAction, DeleteCarRequestAction, RefreshCarsRequestAction} from '../../car-tool.actions';

@Component({
  selector: 'app-car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css']
})
export class CarHomeComponent implements OnInit {

  public cars$: Observable<Car[]>;

  constructor(
    private store: Store<CarToolState>,
  ) { }

  ngOnInit() {
    this.cars$ = this.store.pipe(select('cars'));
    this.store.dispatch(new RefreshCarsRequestAction());
  }

  doDeleteCar(carId: number) {
    this.store.dispatch(new DeleteCarRequestAction({ carId }));
  }

  doAppendCar(car: Car) {
    this.store.dispatch(new AppendCarRequestAction({ car }));
  }

}
