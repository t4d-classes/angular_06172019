import { Action } from '@ngrx/store';

import { Car } from './models/car';

export enum CarToolActions {
  RefreshCarsRequest = '[CarTool] Refresh Cars Request',
  RefreshCarsDone = '[CarTool] Refresh Cars Done',
  AppendCarRequest = '[CarTool] Append Car Request',
  DeleteCarRequest = '[CarTool] Delete Car Request',
}

export class RefreshCarsRequestAction implements Action {
  type = CarToolActions.RefreshCarsRequest;
}

export class RefreshCarsDoneAction implements Action {
  type = CarToolActions.RefreshCarsDone;
  constructor(public payload: { cars: Car[] }) { }
}

export class AppendCarRequestAction implements Action {
  type = CarToolActions.AppendCarRequest;
  constructor(public payload: { car: Car }) { }
}

export class DeleteCarRequestAction implements Action {
  type = CarToolActions.DeleteCarRequest;
  constructor(public payload: { carId: number}) { }
}

export type CarToolActionsUnion =
  RefreshCarsRequestAction |
  RefreshCarsDoneAction |
  AppendCarRequestAction |
  DeleteCarRequestAction;
