import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { ReactiveFormsModule } from '@angular/forms';

import { CarHomeComponent } from './components/car-home/car-home.component';
import { CarTableComponent } from './components/car-table/car-table.component';

import { carsReducer } from './car-tool.reducers';
import { CarToolEffects } from './car-tool.effects';
import { CarFormComponent } from './components/car-form/car-form.component';

@NgModule({
  declarations: [CarHomeComponent, CarTableComponent, CarFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forRoot({ cars: carsReducer }),
    EffectsModule.forRoot([ CarToolEffects ]),
    StoreDevtoolsModule.instrument(),
  ],
  exports: [CarHomeComponent]
})
export class CarToolModule { }
