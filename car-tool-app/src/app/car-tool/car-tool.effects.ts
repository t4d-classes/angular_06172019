import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

import { CarsService } from './services/cars.service';
import {
  CarToolActions,
  CarToolActionsUnion,
  RefreshCarsRequestAction,
  RefreshCarsDoneAction,
  AppendCarRequestAction,
  DeleteCarRequestAction,
} from './car-tool.actions';

@Injectable({
  providedIn: 'root',
})
export class CarToolEffects {

  constructor(
    private carsSvc: CarsService,
    private actions$: Actions<CarToolActionsUnion>,
  ) {}

  @Effect()
  refreshCars$: Observable<RefreshCarsDoneAction> = this.actions$.pipe(
    ofType(CarToolActions.RefreshCarsRequest),
    switchMap(() => {
      return this.carsSvc.all().pipe(
        map(cars => new RefreshCarsDoneAction({ cars })),
      );
    }),
  );

  @Effect()
  appendCar$: Observable<RefreshCarsRequestAction> = this.actions$.pipe(
    ofType(CarToolActions.AppendCarRequest),
    switchMap((action: AppendCarRequestAction) => {
      return this.carsSvc.append(action.payload.car).pipe(
        map(() => new RefreshCarsRequestAction()),
      );
    }),
  );

  @Effect()
  deleteCar$: Observable<RefreshCarsRequestAction> = this.actions$.pipe(
    ofType(CarToolActions.DeleteCarRequest),
    switchMap((action: DeleteCarRequestAction) => {
      return this.carsSvc.delete(action.payload.carId).pipe(
        map(() => new RefreshCarsRequestAction()),
      );
    }),
  );

}
