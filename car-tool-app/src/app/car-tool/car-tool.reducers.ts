import { Car } from './models/car';
import { CarToolActions, CarToolActionsUnion, RefreshCarsDoneAction } from './car-tool.actions';

export const carsReducer = (state: Car[] = [], action: CarToolActionsUnion) => {

  if (action.type === CarToolActions.RefreshCarsDone) {
    return (action as RefreshCarsDoneAction).payload.cars;
  } else {
    return state;
  }

};
