import {
  AppendColorAction,
  ColorToolActions,
  ColorToolActionsUnion,
  DeleteColorAction,
  MoveColorAction,
  MoveDirection,
  ToggleFavoriteColorAction
} from './color-tool.actions';

import {Color} from './color-tool/models/color';

const appendColor = (state: Color[]  = [], action: AppendColorAction) => {

  const newColor = {
    name: action.payload.colorName,
    sortOrder: state.length,
    isFavorite: false,
    id: Math.max(...state.map(c => c.id), 0) + 1,
  };

  return state.concat(newColor);
};

const deleteColor = (state: Color[]  = [], action: DeleteColorAction) => {

  return state.filter(c => c.id !== action.payload.colorId);
};

const moveColor = (state: Color[]  = [], action: MoveColorAction) => {

  const colorToMoveIndex: number = state.findIndex(
    c => c.id === action.payload.colorId);
  const colorToMove: Color = state[colorToMoveIndex];

  let swapColorIndex;
  let newColorToMove;
  let newSwapColor;

  if (action.payload.direction === MoveDirection.Up) {

    const newSortOrder = colorToMove.sortOrder - 1;

    newColorToMove = {
      ...colorToMove,
      sortOrder: newSortOrder,
    };

    swapColorIndex = state.findIndex(
      c => c.sortOrder === newSortOrder);
    if (swapColorIndex > -1) {
      newSwapColor = {
        ...state[swapColorIndex],
        sortOrder: newSortOrder + 1,
      };
    }

  } else {

    const newSortOrder = colorToMove.sortOrder + 1;

    newColorToMove = {
      ...colorToMove,
      sortOrder: newSortOrder,
    };

    swapColorIndex = state.findIndex(
      c => c.sortOrder === newSortOrder);
    if (swapColorIndex !== -1) {
      newSwapColor = {
        ...state[swapColorIndex],
        sortOrder: newSortOrder - 1,
      };
    }
  }

  const newColors = state.concat();
  newColors[colorToMoveIndex] = newColorToMove;
  if (swapColorIndex > -1) {
    newColors[swapColorIndex] = newSwapColor;
  }

  return newColors;
};

export const toggleFavoriteColor = (
  state: Color[]  = [],
  action: ToggleFavoriteColorAction
) => {

  const colorIndex = state.findIndex(
    c => c.id === action.payload.colorId);

  const newColor: Color = {
    ...state[colorIndex],
    isFavorite: !state[colorIndex].isFavorite,
  };

  const newColors = state.slice();
  newColors[colorIndex] = newColor;

  return newColors;
};


export const colorsReducer = (
  state: Color[]  = [],
  action: ColorToolActionsUnion
) => {

  switch (action.type) {
    case ColorToolActions.AppendColor:
      return appendColor(state, action as AppendColorAction);
    case ColorToolActions.DeleteColor:
      return deleteColor(state, action as DeleteColorAction);
    case ColorToolActions.DeleteAllColors:
      return [];
    case ColorToolActions.MoveColor:
      return moveColor(state, action as MoveColorAction);
    case ColorToolActions.ToggleFavoriteColor:
      return toggleFavoriteColor(state, action as ToggleFavoriteColorAction);
    default:
      return state;
  }

};
