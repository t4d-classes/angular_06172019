import { Action } from '@ngrx/store';

export enum MoveDirection {
  Up,
  Down,
}

export enum ColorToolActions {
  AppendColor = '[ColorTool] Append Color',
  DeleteColor = '[ColorTool] Delete Color',
  DeleteAllColors = '[ColorTool] Delete All Colors',
  MoveColor = '[ColorTool] Move Color',
  ToggleFavoriteColor = 'ColorTool] Favorite Color',
}

export class AppendColorAction implements Action {
  type = ColorToolActions.AppendColor;
  constructor(public payload: { colorName: string }) { }
}

export class DeleteColorAction implements Action {
  type = ColorToolActions.DeleteColor;
  constructor(public payload: { colorId: number }) { }
}

export class DeleteAllColorsAction implements Action {
  type = ColorToolActions.DeleteAllColors;
}

export class MoveColorAction implements Action {
  type = ColorToolActions.MoveColor;
  constructor(public payload: {
    colorId: number,
    direction: MoveDirection,
  }) { }
}

export class ToggleFavoriteColorAction implements Action {
  type = ColorToolActions.ToggleFavoriteColor;
  constructor(public payload: { colorId: number }) { }
}

export type ColorToolActionsUnion =
  AppendColorAction |
  DeleteColorAction |
  DeleteAllColorsAction |
  MoveColorAction |
  ToggleFavoriteColorAction;
