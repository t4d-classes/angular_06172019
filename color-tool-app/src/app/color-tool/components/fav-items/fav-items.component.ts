import { Component, Input } from '@angular/core';

@Component({
  selector: 'fav-items',
  templateUrl: './fav-items.component.html',
  styleUrls: ['./fav-items.component.css']
})
export class FavItemsComponent {

  @Input()
  pluralItemName = 'Item';

  @Input()
  items: string[] = [];

  get favItemsList() {
    return this.items.join(', ');
  }
}
