import {
  Component, OnInit,
  Input, ChangeDetectionStrategy,
  EventEmitter, Output,
} from '@angular/core';

import { ListItem } from '../../models/list-item';

@Component({
  selector: 'unordered-list',
  templateUrl: './unordered-list.component.html',
  styleUrls: ['./unordered-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnorderedListComponent implements OnInit {

  @Input()
  items: ListItem [] = [];

  @Output()
  deleteItem = new EventEmitter<number>();

  @Output()
  deleteAllItems = new EventEmitter<void>();

  @Output()
  toggleItem = new EventEmitter<number>();

  @Output()
  moveItemUp = new EventEmitter<number>();

  @Output()
  moveItemDown = new EventEmitter<number>();

  constructor() { }

  ngOnInit() { }
}
