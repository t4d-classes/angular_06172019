import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import {
  ToolHeaderComponent
} from './components/tool-header/tool-header.component';
import {
  UnorderedListComponent
} from './components/unordered-list/unordered-list.component';
import {
  ColorFormComponent
} from './components/color-form/color-form.component';
import { FavItemsComponent } from './components/fav-items/fav-items.component';

@NgModule({
  declarations: [
    ToolHeaderComponent,
    UnorderedListComponent,
    ColorFormComponent,
    FavItemsComponent,
  ],
  exports: [
    ToolHeaderComponent,
    UnorderedListComponent,
    ColorFormComponent,
    FavItemsComponent,
  ],
  imports: [
    CommonModule, ReactiveFormsModule,
  ]
})
export class ColorToolModule { }
