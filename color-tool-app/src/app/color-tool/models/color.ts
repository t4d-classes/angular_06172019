export interface Color {
  id?: number;
  name: string;
  isFavorite: boolean;
  sortOrder: number;
}
