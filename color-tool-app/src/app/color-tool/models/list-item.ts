export interface ListItem {
  id: number;
  item: string;
  toggled: boolean;
}
