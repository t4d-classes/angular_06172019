import {createSelector} from '@ngrx/store';
import {ColorToolState} from './color-tool.state';
import {Color} from './color-tool/models/color';
import {ListItem} from './color-tool/models/list-item';

export const sortColorsSelector = createSelector(
  (state: ColorToolState) => state.colors,
  colors => {
    const newColors = colors.slice();
    newColors.sort((a: Color, b: Color) => {
      return a.sortOrder - b.sortOrder;
    });
    return newColors.map(c => {
      return {
        id: c.id,
        item: c.name,
        toggled: c.isFavorite,
      } as ListItem;
    });
  },
);

export const favColorsSelector = createSelector(
  (state: ColorToolState) => state.colors,
  colors => colors
    .filter(color => color.isFavorite)
    .map(color => color.name)
    .sort(),
);
