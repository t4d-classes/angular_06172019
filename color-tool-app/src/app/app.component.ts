import {Component, OnInit} from '@angular/core';
import {Store, select} from '@ngrx/store';
import {Observable} from 'rxjs';

import {ColorToolState} from './color-tool.state';
import {
  AppendColorAction, DeleteColorAction, DeleteAllColorsAction,
  MoveColorAction, MoveDirection, ToggleFavoriteColorAction,
} from './color-tool.actions';
import {ListItem} from './color-tool/models/list-item';

import {favColorsSelector, sortColorsSelector} from './color-tool.selectors';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  colors$: Observable<ListItem[]>;
  favColors$: Observable<string[]>;

  constructor(
    private store: Store<ColorToolState>,
  ) { }

  ngOnInit(): void {
    this.colors$ = this.store.pipe(select(sortColorsSelector));
    this.favColors$ = this.store.pipe(select(favColorsSelector));
  }

  doAddColor(colorName: string) {
    this.store.dispatch(new AppendColorAction({ colorName }));
  }

  doDeleteColor(colorId: number) {
    this.store.dispatch(new DeleteColorAction({ colorId }));
  }

  doDeleteAllColors() {
    this.store.dispatch(new DeleteAllColorsAction());
  }

  doMoveColorUp(colorId) {
    this.store.dispatch(
      new MoveColorAction({
        colorId,
        direction: MoveDirection.Up
      }));
  }

  doMoveColorDown(colorId) {
    this.store.dispatch(
      new MoveColorAction({
        colorId,
        direction: MoveDirection.Down
      }));
  }

  doToggleFavoriteColor(colorId) {
    this.store.dispatch(
      new ToggleFavoriteColorAction({ colorId })
    );
  }
}
