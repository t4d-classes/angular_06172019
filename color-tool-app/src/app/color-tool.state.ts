import {Color} from './color-tool/models/color';

export interface ColorToolState {
  colors: Color[];
}
