export interface CalcToolState {
  result: number;
  history: { type: string, value: number }[];
}
