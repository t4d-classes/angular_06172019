import { Action } from '@ngrx/store';

export interface ValueAction extends Action {
  value: number;
}

export enum CalcToolActions {
  Add = '[CalcTool] Add',
  Subtract = '[CalcTool] Subtract',
  Clear = '[CalcTool] Clear'
}

export class AddAction implements ValueAction {
  type = CalcToolActions.Add;
  // public value: number;
  // constructor(value: number) {
  //   this.value = value;
  // }
  constructor(public value: number) { }
}

export class SubtractAction implements ValueAction {
  type = CalcToolActions.Subtract;
  constructor(public value: number) { }
}

export class ClearAction implements Action {
  type = CalcToolActions.Clear;
}
export type CalcToolActionsUnion = AddAction | SubtractAction | ClearAction;

