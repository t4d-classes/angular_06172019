import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { resultReducer, historyReducer } from './calc-tool.reducers';
import { CalcFormComponent } from './components/calc-form/calc-form.component';

@NgModule({
  declarations: [CalcFormComponent],
  exports: [CalcFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    // initial state tree: { result: 0, history: [] }
    StoreModule.forRoot({
      result: resultReducer,
      history: historyReducer,
    }),
    StoreDevtoolsModule.instrument(),
  ],
})
export class CalcToolModule { }
