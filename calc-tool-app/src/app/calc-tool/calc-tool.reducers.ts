import { CalcToolActions, CalcToolActionsUnion, AddAction, SubtractAction, ValueAction } from './calc-tool.actions';

export const resultReducer = (
  state = 0,
  action: CalcToolActionsUnion
) => {

  switch (action.type) {
    case CalcToolActions.Add:
      return state + (action as AddAction).value;
    case CalcToolActions.Subtract:
      return state - (action as SubtractAction).value;
    case CalcToolActions.Clear:
      return 0;
    default:
      return state;
  }

};

export const historyReducer = (state = [], action: CalcToolActionsUnion) => {
  switch (action.type) {
    case CalcToolActions.Add:
    case CalcToolActions.Subtract:
      const valueAction = action as ValueAction;
      return state.concat({ type: valueAction.type, value: valueAction.value })
    case CalcToolActions.Clear:
      return [];
    default:
      return state;
  }
};

