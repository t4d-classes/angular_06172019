import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store, select, createSelector } from '@ngrx/store';

import { CalcToolState } from '../../CalcToolState';
import { AddAction, SubtractAction, ClearAction } from '../../calc-tool.actions';

const historyLengthSelector = createSelector(
  (state: CalcToolState) => state.history,
  history => history.length,
);

@Component({
  selector: 'app-calc-form',
  templateUrl: './calc-form.component.html',
  styleUrls: ['./calc-form.component.css']
})
export class CalcFormComponent implements OnInit {

  result$: Observable<number>;
  history$: Observable<string[]>;
  historyLength$: Observable<number>;

  calcForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store<CalcToolState>,
  ) { }

  ngOnInit() {
    this.calcForm = this.fb.group({
      numInput: 0,
    });

    this.result$ = this.store.pipe(select('result'));
    this.history$ = this.store.pipe(select(state =>
      state.history.map(entry => entry.type + ' ' + entry.value)));

    this.historyLength$ = this.store.pipe(select(historyLengthSelector));
    // this.historyLength$ = this.store.pipe(select(state => state.history.length));
  }

  doAdd() {
    this.store.dispatch(new AddAction(this.calcForm.value.numInput));
  }

  doSubtract() {
    this.store.dispatch(new SubtractAction(this.calcForm.value.numInput));
  }

  doClear() {
    this.store.dispatch(new ClearAction());
  }
}
