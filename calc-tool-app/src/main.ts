import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

// const nums = [1, 2, 3, 4, 5];

// // add to the end
// const newNums = nums.concat(6);
// // const newNums = [ ...nums, 6 ];

// console.log(nums);
// console.log(newNums);

// // remove from the beginning
// const newNums2 = nums.slice(1);
// console.log(newNums2);

// // remove from the end
// const newNums3 = nums.slice(0, nums.length - 1);
// console.log(newNums3);

// const person = {
//   fn: 'Bob',
//   ln: 'Stackhouse',
// };

// person.ln = 'Smith';

// const newPerson = Object.assign({}, person, { ln: 'Smith'});

// const newPerson = {
//   ...person,
//   ln: 'Smith',
// };

// const [ first, second, ...remaining ] = [1, 2, 3, 4, 5];

// console.log(remaining);

// const fn = (a, b, ...c) => {
//   console.log(c);
// }

// fn(...[1, 2, 3, 4, 5]);
