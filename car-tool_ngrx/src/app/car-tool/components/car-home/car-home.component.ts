import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store, select } from '@ngrx/store';

import { AppState } from '../../../app-state';
import { Car } from '../../models/car';
import {
  RefreshCarsRequestAction, AppendCarRequestAction,
  ReplaceCarAction, DeleteCarAction,
  EditCarAction, CancelCarAction } from '../../car.actions';

@Component({
  selector: 'car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css']
})
export class CarHomeComponent implements OnInit {
  cars$ = this.store.pipe(select(state => state.cars));
  editCarId$ = this.store.pipe(select('editCarId'));

  constructor(
    private store: Store<AppState>,
    // private httpClient: HttpClient,
  ) { }

  ngOnInit() {
    this.store.dispatch(new RefreshCarsRequestAction());
    // this.httpClient
    //   .get<{ id: number; name: string }[]>('http://localhost:4250/colors')
    //   .toPromise()
    //   .then(colors => {
    //     console.log(colors);
    //   });

    // this.httpClient
    //   .post<{ id: number; name: string }>('http://localhost:4250/colors', { name: 'purple' })
    //   .toPromise()
    //   .then(color => console.log(color));
  }

  doAddCar(car: Car) {
    this.store.dispatch(new AppendCarRequestAction(car));
  }

  doReplaceCar(car: Car) {
    this.store.dispatch(new ReplaceCarAction(car));
  }

  doDeleteCar(carId: number) {
    this.store.dispatch(new DeleteCarAction(carId));
  }

  doEditCar(carId: number) {
    this.store.dispatch(new EditCarAction(carId));
  }

  doCancelCar() {
    this.store.dispatch(new CancelCarAction());
  }
}
