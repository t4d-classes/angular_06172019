import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { CarToolModule } from './car-tool/car-tool.module';

import { AppComponent } from './app.component';
import { carsReducer, editCarIdReducer } from './car-tool/car.reducers';
import { CarEffects } from './car-tool/car.effects';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({
      cars: carsReducer,
      editCarId: editCarIdReducer,
    }),
    EffectsModule.forRoot([ CarEffects ]),
    StoreDevtoolsModule.instrument(),
    CarToolModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
